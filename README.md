# README

A collection of Ansible roles to automate installation of crashplan on linux machines.

## Requirements

* JDK8. Either OpenJDK or the Oracle JDK is fine, but I heard that the Oracle JDK performs much better on the Raspberry Pi
* These unix utilities: `expect`, `cpio`, and `sed`
* If installing on ArchLinux ARM, you will also need to install the following packages:
    * `binutils` (for the `ar` utility)
    * `libffi`

## Usage

Use the following (mostly) wrapper roles to install for the respective distribution:

* `crashplan_raspbian`
* `crashplan_archlinuxarm`

See `roles/crashplan__vars/defaults/main.yml` for a list of variables that you can set to customise the installation.

The intended usage of the repo is that you would pull this in as a git subtree, and you can set the `roles_path` in your `ansible.cfg` to include the roles in this repo.

## Advance Usage

To just install crashplan itself, use one of these modules:

* `crashplan_install` -- Just do the basic download and install of the crashplan tarball
* `crashplan_install_rpi` -- Installing on the Raspberry Pi requires a few tricks and extra steps. _WARNING: Unfortunately, running crashplan on the Raspberry Pi requires some pre-compiled binaries, which is installed by this role. If you don't like that, you can just see what this module does and roll your own._

To install the crashplan service (and/or some helper utilities), use one of these roles:

* `crashplan_service_sysv` -- CrashPlan installs a SysV service by default, this role install some helper scripts to ensure that the service is restarted if, ironically, CrashPlan itself crashes.
* `crashplan_service_systemd` -- Install systemd service for crashplan.

## TODO

* Get this to work for a few popular x86 linux distributions:
  * Ubuntu
  * Fedora/Centos
  * ArchLinux
