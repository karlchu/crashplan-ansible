# crashplan__var

*This is not a top-level role*

All user configurable variables are described here. See inline documentation
in `defaults/main.yml`.
