#`crashplan_upgrade_limit`

This role creates a file-based loop files system and map it to the `upgrade` directory under the crashplan install directory.

Newer versions of Crashplan attempts to automatically upgrade itself.
The auto upgrade does not appear to work on a raspbian machine.
As a result, it downloads over 40MB **every minute** and fills up a 32GB SD card in less than 12 hours.
This role creates a loop file system and limits its size to 256MB, so that if the upgrade fails, it does not fill up the drive and bring down the whole system.
